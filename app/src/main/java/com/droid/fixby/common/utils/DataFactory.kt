package com.droid.fixby.common.utils

import com.droid.fixby.R
import com.droid.fixby.screens.dashboard.models.*

class DataFactory {
    companion object {

        fun getSaloonList() = ArrayList<SaloonModel>().apply {
            add(SaloonModel(R.drawable.icon_saloon_sanasara))
            add(SaloonModel(R.drawable.icon_saloon_sabs))
            add(SaloonModel(R.drawable.icon_saloon_sara))
            add(SaloonModel(R.drawable.icon_saloon_blush))
            add(SaloonModel(R.drawable.icon_saloon_kashees))
            add(SaloonModel(R.drawable.icon_saloon_depilex))
            add(SaloonModel(R.drawable.icon_saloon_natasha))
        }

        fun getTrendingSaloonList() = ArrayList<SaloonModel>().apply {
            add(SaloonModel(R.drawable.icon_saloon_depilex))
            add(SaloonModel(R.drawable.icon_saloon_natasha))
            add(SaloonModel(R.drawable.icon_saloon_blush))
            add(SaloonModel(R.drawable.icon_saloon_sara))
            add(SaloonModel(R.drawable.icon_saloon_kashees))

        }

        fun getRestaurantsSaloonList() = ArrayList<SaloonModel>().apply {
            add(SaloonModel(R.drawable.icon_bamboo_logo))
            add(SaloonModel(R.drawable.logo_ambrosia))
            add(SaloonModel(R.drawable.icon_delizia))
            add(SaloonModel(R.drawable.logo_rosati))
            add(SaloonModel(R.drawable.icon_kababjees))
            add(SaloonModel(R.drawable.icon_xanders))
            add(SaloonModel(R.drawable.icon_koel))

        }

        fun getRestaurantsPhotosList() = ArrayList<SaloonModel>().apply {
            add(SaloonModel(R.drawable.rest_photo1))
            add(SaloonModel(R.drawable.rest_photo2))
            add(SaloonModel(R.drawable.rest_photo3))
            add(SaloonModel(R.drawable.rest_photo4))
            add(SaloonModel(R.drawable.rest_photo5))
            add(SaloonModel(R.drawable.rest_photo6))
        }

        fun getServicesList() = ArrayList<ServiceModel>().apply {
            add(ServiceModel(R.drawable.icon_womanwithlonghair, "Hair Stylist"))
            add(ServiceModel(R.drawable.mask_group_9, "Henna"))
            add(ServiceModel(R.drawable.icon_group2512, "Party Makeup"))
            add(ServiceModel(R.drawable.icon_makeup, "Bridal Makeup"))
            add(ServiceModel(R.drawable.icon_manicure, "Manicure"))
            add(ServiceModel(R.drawable.pedicure, "Pedicure"))

            add(ServiceModel(R.drawable.icon_wax, "Waxing"))
            add(ServiceModel(R.drawable.icon_025cleanface, "Cleansing"))
            add(ServiceModel(R.drawable.icon_facialmask, "Facial"))
            add(ServiceModel(R.drawable.icon_beautyhandcream, "Massage"))

        }

        fun getRestaurantServicesList() = ArrayList<ServiceModel>().apply {
            add(ServiceModel(R.drawable.icon_table, "Table"))
            add(ServiceModel(R.drawable.icon_weddingrings, "Wedding Dinner"))
            add(ServiceModel(R.drawable.icon_cake1, "Birthday Party"))
            add(ServiceModel(R.drawable.icon_restaurant, "Office Lunch"))
            add(ServiceModel(R.drawable.icon_stars, "Friends Party"))
            add(ServiceModel(R.drawable.icon_family, "Family"))

        }

        fun getStylistList() = ArrayList<StylistModel>().apply {
            add(StylistModel(R.drawable.icon_fd, "Erick Su"))
            add(StylistModel(R.drawable.icon_maskgroup4, "Alberto Silva"))
            add(StylistModel(R.drawable.mask_group_4, "Jesse Brown"))
            add(StylistModel(R.drawable.icon_component1, "Any"))


        }

        fun getTimeSlotsList() = ArrayList<TimeSlotModel>().apply {
            add(TimeSlotModel("9 AM - 12 PM"))
            add(TimeSlotModel("12 PM - 2 PM"))
            add(TimeSlotModel("3 PM - 6 PM"))
            add(TimeSlotModel("6 PM - 9 PM"))

        }

        fun getMyAppointmentList() = ArrayList<AppointmentModel>().apply {
            add(AppointmentModel("9 AM - 12 PM"))
            add(AppointmentModel("12 PM - 2 PM"))
            add(AppointmentModel("3 PM - 6 PM"))
            add(AppointmentModel("6 PM - 9 PM"))

        }

        fun getReviews() = ArrayList<ReviewModel>().apply {
            add(ReviewModel("John Snow", "Amazing Food"))
            add(ReviewModel("John Snow", "Amazing Food"))
            add(ReviewModel("John Snow", "Amazing Food"))
            add(ReviewModel("John Snow", "Amazing Food"))
            add(ReviewModel("John Snow", "Amazing Food"))
            add(ReviewModel("John Snow", "Amazing Food"))

        }

    }
}