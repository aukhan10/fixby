package com.droid.fixby.common.utils

import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.droid.fixby.common.activities.BaseActivity

fun RecyclerView.initVertical(mContext: BaseActivity) {
    val mlayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
    layoutManager = mlayoutManager
}

fun RecyclerView.initHorizontal(mContext: BaseActivity) {
    val mlayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
    layoutManager = mlayoutManager
}

fun RecyclerView.initGrid(mContext: BaseActivity) {
    val mlayoutManager = GridLayoutManager(mContext, 2)
    layoutManager = mlayoutManager
}

fun Fragment.getName(): String {
    return javaClass.name
}