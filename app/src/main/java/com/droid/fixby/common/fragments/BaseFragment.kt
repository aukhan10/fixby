package com.droid.fixby.common.fragments

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.droid.fixby.common.activities.BaseActivity
import com.droid.fixby.screens.dashboard.activities.MainActivity

abstract class BaseFragment : Fragment() {

    var mContext: BaseActivity? = null

    var fActivity : MainActivity? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(setLayout(), container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        mContext = (activity as MainActivity).mContext
        fActivity = (activity as MainActivity)
        inits()
    }

    protected abstract fun setLayout(): Int

    protected abstract fun inits()

    fun myActivity(): MainActivity? {
        return fActivity
    }
//
    fun replaceFragment(currentFragment: Fragment) {
        myActivity()?.replaceFragment(currentFragment)
    }

    fun setTitle(title: String){
        myActivity()?.setTitle(title)
    }
}