package com.droid.fixby.screens.dashboard.models;

public class AppointmentModel {
    public String name;
    public int icon;
    public boolean isSelected = false;

    public AppointmentModel(String name) {
        this.name = name;
    }
}
