package com.droid.fixby.screens.dashboard.fragments

import android.graphics.Typeface
import android.view.View
import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.common.utils.DataFactory
import com.droid.fixby.common.utils.initHorizontal
import com.droid.fixby.common.utils.initVertical
import com.droid.fixby.screens.dashboard.adapters.ReviewsAdapter
import com.droid.fixby.screens.dashboard.adapters.SaloonListAdapter
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.SaloonModel
import kotlinx.android.synthetic.main.fragment_restaurant_detail.*

class RestaurantDetailFragment : BaseFragment() {

    var list = ArrayList<SaloonModel>()

    override fun setLayout(): Int = R.layout.fragment_restaurant_detail

    override fun inits() {

        setTitle(getString(R.string.restaurants))

        list = DataFactory.getRestaurantsPhotosList()

        recyclerPhotos.initHorizontal(mContext!!)
        recyclerPhotos.adapter = SaloonListAdapter(mContext!!, list, R.layout.item_photos, null)

        val reviews = DataFactory.getReviews()
        recyclerReviews.initVertical(mContext!!)
        recyclerReviews.adapter = ReviewsAdapter(mContext!!, reviews)

        btnAbout.setTypeface(null, Typeface.BOLD);
        btnAbout.setOnClickListener {
            layoutAbout.visibility = View.VISIBLE
            recyclerReviews.visibility = View.GONE
            btnAbout.setTypeface(null, Typeface.BOLD);
            btnReview.setTypeface(null, Typeface.NORMAL);
        }

        btnReview.setOnClickListener {
            layoutAbout.visibility = View.GONE
            recyclerReviews.visibility = View.VISIBLE
            btnAbout.setTypeface(null, Typeface.NORMAL);
            btnReview.setTypeface(null, Typeface.BOLD);
        }

        btnBook.setOnClickListener {
            replaceFragment(BookRestaurantFragment())
        }
    }

}