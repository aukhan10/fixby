package com.droid.fixby.screens.prelogin.activities

import android.content.Intent
import com.droid.fixby.screens.dashboard.activities.MainActivity
import com.droid.fixby.R
import com.droid.fixby.common.activities.BaseActivity
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity() {

    override fun setLayout() = R.layout.activity_sign_up

    override fun inits() {
        btnSignUp.setOnClickListener {
            startActivity(MainActivity::class.java)
        }

        txtSignIn.setOnClickListener {
            startActivity(SignInActivity::class.java)

        }
    }
}