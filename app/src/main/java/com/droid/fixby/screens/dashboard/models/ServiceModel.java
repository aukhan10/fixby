package com.droid.fixby.screens.dashboard.models;

public class ServiceModel {
    public String name;
    public int icon;
    public boolean isSelected = false;

    public ServiceModel(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
