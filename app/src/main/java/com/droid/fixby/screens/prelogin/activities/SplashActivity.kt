package com.droid.fixby.screens.prelogin.activities

import android.content.Intent
import com.droid.fixby.screens.dashboard.activities.MainActivity
import com.droid.fixby.R
import com.droid.fixby.common.activities.BaseActivity

class SplashActivity : BaseActivity() {

    override fun setLayout() = R.layout.activity_splash

    override fun inits() {
        object : Thread() {
            override fun run() {
                try {
                    sleep(2000)
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
//                    startActivity(
//                        Intent(applicationContext, SignInActivity::class.java).addFlags(
//                            Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//                        )
//                    )
                    startActivity(SignInActivity::class.java)
                }
            }
        }.start()
    }
}