package com.droid.fixby.screens.dashboard.models;

public class ReviewModel {
    public String name;
    public String detail;

    public ReviewModel(String name, String detail) {
        this.name = name;
        this.detail = detail;
    }
}
