package com.droid.fixby.screens.dashboard.interfaces

interface OnItemClickListener {
    fun onItemClick(model: Any?, position: Int)
}