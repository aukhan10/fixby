package com.droid.fixby.screens.dashboard.fragments

import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.common.utils.DataFactory
import com.droid.fixby.common.utils.initHorizontal
import com.droid.fixby.common.utils.initVertical
import com.droid.fixby.screens.dashboard.adapters.SaloonListAdapter
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.SaloonModel
import kotlinx.android.synthetic.main.fragment_saloons.*

class SaloonsFragment : BaseFragment(), OnItemClickListener {

    var list = ArrayList<SaloonModel>()

    override fun setLayout(): Int = R.layout.fragment_saloons

    override fun inits() {

        setTitle(getString(R.string.salons))

        list = DataFactory.getTrendingSaloonList()
        recyclerTrending.initHorizontal(mContext!!)
        recyclerTrending.adapter = SaloonListAdapter(mContext!!, list, R.layout.item_trending,
            object : OnItemClickListener {
                override fun onItemClick(model: Any?, position: Int) {
                    replaceFragment(BookAppointmentFragment())
                }
            })

        list = DataFactory.getSaloonList()
        recyclerList.initVertical(mContext!!)
        recyclerList.adapter = SaloonListAdapter(mContext!!, list, R.layout.item_saloon, this)
    }

    override fun onItemClick(model: Any?, position: Int) {
        var item = model as SaloonModel
        replaceFragment(BookAppointmentFragment())
    }


}