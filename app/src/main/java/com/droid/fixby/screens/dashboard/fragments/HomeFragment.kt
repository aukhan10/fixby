package com.droid.fixby.screens.dashboard.fragments

import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.screens.dashboard.activities.MainActivity
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_home

    override fun inits() {
        layoutSalons.setOnClickListener {

            replaceFragment(SaloonsFragment())
        }
        layoutRestaurants.setOnClickListener {
            replaceFragment(RestaurantsFragment())
        }
    }
}