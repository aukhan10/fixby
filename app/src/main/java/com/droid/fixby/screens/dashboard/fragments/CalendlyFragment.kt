package com.droid.fixby.screens.dashboard.fragments

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.common.utils.initVertical
//import com.droid.fixby.screens.dashboard.adapters.EventListAdapter
//import com.droid.fixby.screens.dashboard.adapters.SaloonListAdapter
//import com.kizitonwose.calendarview.model.CalendarDay
//import com.kizitonwose.calendarview.model.DayOwner
//import com.kizitonwose.calendarview.ui.DayBinder
import kotlinx.android.synthetic.main.fragment_calendly.*
import kotlinx.android.synthetic.main.fragment_saloons.*
import java.time.Month
import java.time.YearMonth
import java.time.temporal.WeekFields
import java.util.*
import kotlin.collections.ArrayList


class CalendlyFragment : BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_calendly

    @RequiresApi(Build.VERSION_CODES.O)
    override fun inits() {
        setTitle(getString(R.string.calendly))


//        rvEvents.initVertical(mContext!!)
//        rvEvents.adapter = EventListAdapter(mContext!!)
//
//
//        calendarView.dayBinder = object : DayBinder<DayViewContainer> {
//            override fun create(view: View) = DayViewContainer(view)
//            override fun bind(container: DayViewContainer, day: CalendarDay) {
//                container.textView.text = day.date.dayOfMonth.toString()
//                if (day.owner == DayOwner.THIS_MONTH) {
//                    container.textView.setTextColor(Color.DKGRAY)
//                    val month = Month.of(day.date.monthValue)
//                    tvMonth.text = "$month ${day.date.year}"
//                    if (day.date.dayOfMonth == 15 && day.date.monthValue == 12) {
//                        container.eventsDot.visibility = View.VISIBLE
//                    }
//                } else {
//                    container.textView.setTextColor(Color.LTGRAY)
//                }
//                container.textView.setOnClickListener {
////                    dateTV.text = "${day.date.dayOfWeek} ${day.date.dayOfMonth},${day.date.year}"
//                }
//            }
//        }
//
//        calendarView.monthScrollListener = {
////            val month = Month.of(it.month)
////            monthAndYearTV.text = "${month} ${it.yearMonth.year}"
//        }
//
//        val currentMonth = YearMonth.now()
//        val firstMonth = currentMonth.minusMonths(10)
//        val lastMonth = currentMonth.plusMonths(10)
//        val firstDayOfWeek = WeekFields.of(Locale.getDefault()).firstDayOfWeek
//        calendarView.setup(firstMonth, lastMonth, firstDayOfWeek)
//        calendarView.scrollToMonth(currentMonth)


//        mContext?.let {
//            val list = readCalendarEvent(it)
//            list.size
//        }
    }

//    var nameOfEvent = ArrayList<String>()
//    var startDates = ArrayList<String>()
//    var endDates = ArrayList<String>()
//    var descriptions = ArrayList<String>()
//
//
//    fun readCalendarEvent(context: Context): ArrayList<String> {
//        val cursor: Cursor? = context.getContentResolver()
//            .query(
//                Uri.parse("content://com.android.calendar/events"), arrayOf(
//                    "calendar_id", "title", "description",
//                    "dtstart", "dtend", "eventLocation"
//                ), null,
//                null, null
//            )
//        cursor?.moveToFirst()
//        // fetching calendars name
//        val CNames = cursor?.getCount()?.let { arrayOfNulls<String>(it) }
//
//        // fetching calendars id
//        nameOfEvent.clear()
//        startDates.clear()
//        endDates.clear()
//        descriptions.clear()
//        for (i in CNames?.indices!!) {
//            cursor?.getString(1)?.let { nameOfEvent.add(it) }
//            getDate(cursor?.getString(3).toLong())?.let { startDates.add(it) }
//            getDate(cursor.getString(4).toLong())?.let { endDates.add(it) }
//            descriptions.add(cursor?.getString(2))
//            CNames[i] = cursor?.getString(1)
//            cursor?.moveToNext()
//        }
//        return nameOfEvent
//    }
//
//    fun getDate(milliSeconds: Long): String? {
//        val formatter = SimpleDateFormat(
//            "dd/MM/yyyy hh:mm:ss a"
//        )
//        val calendar: Calendar = Calendar.getInstance()
//        calendar.setTimeInMillis(milliSeconds)
//        return formatter.format(calendar.getTime())
//    }
}