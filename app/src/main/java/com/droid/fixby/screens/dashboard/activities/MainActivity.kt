package com.droid.fixby.screens.dashboard.activities

import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.droid.fixby.R
import com.droid.fixby.common.activities.BaseActivity
import com.droid.fixby.common.utils.getName
import com.droid.fixby.screens.dashboard.fragments.*
import com.droid.fixby.screens.prelogin.activities.SignInActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.header.*

class MainActivity : BaseActivity() {

    override fun setLayout() = R.layout.activity_main

    override fun inits() {


        setBottomBar()
        setSideNavMenu()

        llMenu1.setOnClickListener {
            layoutDrawer.openDrawer(GravityCompat.START)
        }

        replaceFragment(HomeFragment())
    }

    fun setTitle(title: String) {
        tv_title.text = title
    }

//    fun openAndCloseDrawer(menu: Boolean) {
//        if (layoutDrawer.isDrawerOpen(GravityCompat.START)) {
//            layoutDrawer.closeDrawer(GravityCompat.START)
//        } else if (menu) {
//            layoutDrawer.openDrawer(GravityCompat.START)
//        }
//    }

    private fun setSideNavMenu() {
        navView.setNavigationItemSelectedListener(object :
            NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.nav_home -> replaceFragment(HomeFragment())
                    R.id.nav_profile -> replaceFragment(ProfileFragment())
                    R.id.nav_terms -> replaceFragment(TermsFragment())
                    R.id.nav_contact -> replaceFragment(ContactUsFragment())
                    R.id.nav_setting -> showToast("Clicked item four")
                    R.id.nav_logout -> logOut()
                }
                layoutDrawer.closeDrawer(GravityCompat.START)
                return true
            }

        })
    }

    private fun logOut(){
        startActivity(SignInActivity::class.java)
    }

    private fun setBottomBar() {
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.itemHome -> {
                    replaceFragment(HomeFragment())
                    true
                }
                R.id.itemAppoinments -> {

                    replaceFragment(AppointmentsFragment())
                    true
                }
                R.id.itemNotifier -> {

                    replaceFragment(NotifierFragment())
                    true
                }
                R.id.itemCalendly -> {

                    replaceFragment(CalendlyFragment())
                    true
                }
                else -> false
            }


        }
    }

    public fun replaceFragment(currentFragment: Fragment) {

        setTitle("")

        var activeFragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        var activeFragmentName = activeFragment?.javaClass?.name
//        showToast("Current Fragment is " + activeFragmentName)

        if (!activeFragmentName.equals(currentFragment.javaClass.name)) {


            val backStateName: String = currentFragment.getName()
            val manager: FragmentManager = supportFragmentManager

            var fragmentPopped = manager.popBackStackImmediate(backStateName, 0)

            if (!fragmentPopped) {//fragment not in back stack, create it.
                val ft: FragmentTransaction = manager.beginTransaction()
//                ft.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                ft.replace(R.id.content_frame, currentFragment)
                ft.addToBackStack(backStateName)
                ft.commit()
            }
        }
    }

    var doubleBackToExitPressedOnce: Boolean = false
    override fun onBackPressed() {
        if (layoutDrawer.isDrawerOpen(GravityCompat.START)) {
            layoutDrawer.closeDrawer(GravityCompat.START)
            return
        }

        if (supportFragmentManager.backStackEntryCount == 1) {
            if (doubleBackToExitPressedOnce) {
                finish()
                return
            }

            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "Back again will exit", Toast.LENGTH_SHORT).show()

            android.os.Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)

        } else {
            super.onBackPressed()
        }
    }


}