package com.droid.fixby.screens.dashboard.fragments

import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment

class NotifierFragment: BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_notifier

    override fun inits() {
        setTitle(getString(R.string.notification_title))
    }
}