package com.droid.fixby.screens.dashboard.fragments

import android.content.Intent
import android.net.Uri
import android.view.View
import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import kotlinx.android.synthetic.main.fragment_contact_us.*


class ContactUsFragment : BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_contact_us

    override fun inits() {
        btnChat.setOnClickListener {
            openWhatsApp(it)
        }

        btnCall.setOnClickListener {
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:03314815959")
            startActivity(intent)
        }
    }

    fun openWhatsApp(view: View?) {
        try {
            val text = "This is a test" // Replace with your message.
            val toNumber =
                "923314815959" // Replace with mobile phone number without +Sign or leading zeros, but with country code
            //Suppose your country is India and your phone number is “xxxxxxxxxx”, then you need to send “91xxxxxxxxxx”.
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://api.whatsapp.com/send?phone=$toNumber&text=$text")
            intent.setPackage("com.whatsapp")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}