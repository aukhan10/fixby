package com.droid.fixby.screens.dashboard.fragments

import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment

class SampleFragment: BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_sample

    override fun inits() {

    }
}