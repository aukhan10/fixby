package com.droid.fixby.screens.dashboard.models;

public class TimeSlotModel {
    public String name;
    public int icon;
    public boolean isSelected = false;

    public TimeSlotModel(String name) {
        this.name = name;
    }
}
