package com.droid.fixby.screens.dashboard.fragments

import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment

class TermsFragment : BaseFragment() {

    override fun setLayout(): Int = R.layout.fragment_terms

    override fun inits() {
        myActivity()?.setTitle(getString(R.string.terms_title))
    }
}