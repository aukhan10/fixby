package com.droid.fixby.screens.dashboard.fragments

import android.app.AlertDialog
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.common.utils.DataFactory
import com.droid.fixby.screens.dashboard.adapters.AppointmentsPagerAdapter
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_appointments.*


class AppointmentsFragment : BaseFragment(), View.OnClickListener, OnItemClickListener {

    override fun setLayout(): Int = R.layout.fragment_appointments

    override fun inits() {
        setTitle(getString(R.string.appointments_title))

//        viewPager.setClipToPadding(false);
//        viewPager.setPageMargin(20);
//        viewPager.setPadding(40, 0, 40, 0);
        viewPager.clipToPadding = false;
        viewPager.pageMargin = 12;
        viewPager.adapter =
            AppointmentsPagerAdapter(mContext!!, DataFactory.getMyAppointmentList(), false, this)
        btnUpcoming.setOnClickListener(this)
        btnPast.setOnClickListener(this)
        btnReschedule.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.btnUpcoming -> {
                selectButton(btnUpcoming)
                viewPager.invalidate()
                viewPager.adapter =
                    AppointmentsPagerAdapter(mContext!!, DataFactory.getMyAppointmentList(), false,this)
            }
            R.id.btnPast -> {
                selectButton(btnPast)
                viewPager.invalidate()
                viewPager.adapter =
                    AppointmentsPagerAdapter(mContext!!, DataFactory.getMyAppointmentList(), true,this)
            }
            R.id.btnReschedule -> {
                replaceFragment(BookAppointmentFragment())
            }
        }
    }

    private fun selectButton(tvButton: TextView) {

        btnUpcoming.apply {
            background = ContextCompat.getDrawable(context!!, R.drawable.bg_button_white)
            setTextColor(ContextCompat.getColor(context!!, R.color.primary_green1))
        }
        btnPast.apply {
            background = ContextCompat.getDrawable(context!!, R.drawable.bg_button_white)
            setTextColor(ContextCompat.getColor(context!!, R.color.primary_green1))
        }
        tvButton.apply {
            background = ContextCompat.getDrawable(context!!, R.drawable.bg_button)
            setTextColor(ContextCompat.getColor(context!!, R.color.white))
        }
    }

    override fun onItemClick(model: Any?, position: Int) {
        when (position) {
            0 -> {//Rebook

            }
            1 -> {//Cancel
                AlertDialog.Builder(context)
                    .setMessage("Are you sure you want to cancel this appointment?")
                    .setPositiveButton(
                        "Yes, I m sure"
                    ) { dialog, which ->
                        dialog.dismiss()
                    }
                    .setNegativeButton("No, I don't", null)
                    .show()
            }
        }
    }
}