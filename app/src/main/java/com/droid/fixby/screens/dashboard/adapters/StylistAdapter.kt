package com.droid.fixby.screens.dashboard.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.droid.fixby.R
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.StylistModel
import kotlinx.android.synthetic.main.item_stylist.view.*

class StylistAdapter(
    private val context: Context,
    private val list: ArrayList<StylistModel>,
    private var iface: OnItemClickListener? = null
) : RecyclerView.Adapter<StylistAdapter.ViewHolder>() {
    var selected: RadioButton? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_stylist,
                parent,
                false
            ), iface
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list.get(position)

//        holder.checkBox.setButtonDrawable(R.drawable.custom_checkbox)

        holder.rbStylist.setOnCheckedChangeListener { compoundButton, b ->

            if (b) {
                selected?.isChecked = false
                selected = holder.rbStylist
            }

        }

        model.icon?.let {
            holder.logo.setImageResource(it)
        }
        model.name?.let {
            holder.name.text = it
        }
        holder.item?.setOnClickListener {
            holder.iface?.onItemClick(model, position)
            model.isSelected = !model.isSelected
//            setSelectedUI(holder, model.isSelected)
        }


    }

    fun setSelectedUI(holder: ViewHolder, isSelected: Boolean) {
//        if (isSelected) {
//            holder.logo.setColorFilter(ContextCompat.getColor(context, R.color.white))
//            holder.name.setTextColor(Color.WHITE)
//            holder.item.setBackgroundResource(R.drawable.bg_button)
//        } else {
//            holder.logo.setColorFilter(null)
//            holder.name.setTextColor(context.getColor(R.color.colorTextItem))
//            holder.item.setBackgroundResource(R.drawable.bg_service)
//        }
    }

    class ViewHolder(view: View, var iface: OnItemClickListener?) : RecyclerView.ViewHolder(view) {
        val logo = view.ivImage
        val name = view.txtName
        val item = view.mainView
        val rbStylist = view.rbStylist
    }


}