package com.droid.fixby.screens.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.droid.fixby.R
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.AppointmentModel
import kotlinx.android.synthetic.main.item_pager_appointment.view.*

class AppointmentsPagerAdapter(
    private val context: Context,
    private val list: ArrayList<AppointmentModel>,
    private val isPast: Boolean,
    private val clickListener: OnItemClickListener
) : PagerAdapter() {


    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(context)
        val myImageLayout: View = inflater.inflate(R.layout.item_pager_appointment, view, false)

            myImageLayout.tvButton.apply {
                if (isPast) {
                    text = "  REBOOK  "
                    setOnClickListener {
                        clickListener.onItemClick(null,0)
                    }
                } else {
                    text = "  CANCEL  "
                    setOnClickListener {
                        clickListener.onItemClick(null,1)
                    }
                }
            }

//        val txtDate = myImageLayout.findViewById<TextView>(R.id.txtDate)
//        val txtMonth = myImageLayout.findViewById<TextView>(R.id.txtMonth)
//
//        val str = "" + arrListUserGuide.get(position).trim { it <= ' ' }
//        val splited = str.split("\\s+".toRegex()).toTypedArray()
//
//        txtDate.text = splited[0]
//        txtMonth.text = splited[1]


        view.addView(myImageLayout, 0)
        return myImageLayout
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view.equals(`object`)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}