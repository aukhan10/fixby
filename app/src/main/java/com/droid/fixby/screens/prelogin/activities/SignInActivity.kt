package com.droid.fixby.screens.prelogin.activities

import android.content.Intent
import com.droid.fixby.screens.dashboard.activities.MainActivity
import com.droid.fixby.R
import com.droid.fixby.common.activities.BaseActivity
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : BaseActivity() {

    override fun setLayout() = R.layout.activity_sign_in

    override fun inits() {
        btnSignIn.setOnClickListener {
            startActivity(MainActivity::class.java)
        }

        txtSignUp.setOnClickListener {
            startActivity(SignUpActivity::class.java)
        }
    }
}