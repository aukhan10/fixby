package com.droid.fixby.screens.dashboard.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.droid.fixby.R
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.ReviewModel
import kotlinx.android.synthetic.main.item_review.view.*

class ReviewsAdapter(
    private val context: Context,
    private val list: ArrayList<ReviewModel>,
) : RecyclerView.Adapter<ReviewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_review,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list.get(position)

//        holder.checkBox.setButtonDrawable(R.drawable.custom_checkbox)

        model.detail?.let {
            holder.detail.text = it
        }
        model.name?.let {
            holder.name.text = it
        }


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.txtName
        val detail = view.txtDetail
    }


}