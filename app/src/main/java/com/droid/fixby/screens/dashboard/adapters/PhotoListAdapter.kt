package com.droid.fixby.screens.dashboard.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.SaloonModel
import kotlinx.android.synthetic.main.item_saloon.view.*

class PhotoListAdapter(
    private val context: Context,
    private val list: ArrayList<SaloonModel>,
    private val itemSaloon: Int,
    private var iface: OnItemClickListener? = null
) : RecyclerView.Adapter<PhotoListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                itemSaloon,
                parent,
                false
            ), iface
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list.get(position)

        model.icon?.let {
            holder.logo.setImageResource(it)
        }
//        model.task?.let {
//            holder.task.text = it
//        }
        holder.item?.setOnClickListener {
            holder.iface?.onItemClick(model, position)
        }


    }

    class ViewHolder(view: View, var iface: OnItemClickListener?) : RecyclerView.ViewHolder(view) {
        val logo = view.ivImage
        val item = view.mainView
    }


}