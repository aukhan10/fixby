package com.droid.fixby.screens.dashboard.models;

public class StylistModel {
    public String name;
    public int icon;
    public boolean isSelected = false;

    public StylistModel(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
