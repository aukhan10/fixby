package com.droid.fixby.screens.dashboard.fragments

import android.view.View
import com.droid.fixby.R
import com.droid.fixby.common.fragments.BaseFragment
import com.droid.fixby.common.utils.DataFactory
import com.droid.fixby.common.utils.initGrid
import com.droid.fixby.common.utils.initVertical
import com.droid.fixby.screens.dashboard.adapters.ServicesAdapter
import com.droid.fixby.screens.dashboard.adapters.StylistAdapter
import com.droid.fixby.screens.dashboard.adapters.TimeSlotAdapter
import com.droid.fixby.screens.dashboard.interfaces.OnItemClickListener
import com.droid.fixby.screens.dashboard.models.ServiceModel
import com.droid.fixby.screens.dashboard.models.StylistModel
import com.droid.fixby.screens.dashboard.models.TimeSlotModel
import kotlinx.android.synthetic.main.fragment_book_appointment.*

class BookRestaurantFragment : BaseFragment(), OnItemClickListener {

    private var stepNumber: Int = 1

    var list = ArrayList<ServiceModel>()
    var stylistList = ArrayList<StylistModel>()
    var timeList = ArrayList<TimeSlotModel>()
    override fun setLayout(): Int = R.layout.fragment_book_restaurant

    override fun inits() {
        setTitle(getString(R.string.book_appointment))

        list.addAll(DataFactory.getRestaurantServicesList())

        stylistList.addAll(DataFactory.getStylistList())

        timeList.addAll(DataFactory.getTimeSlotsList())

        showCurrentStep(stepNumber)

        btnNext.setOnClickListener {
            if (stepNumber < 4) {
                stepNumber++
                showCurrentStep(stepNumber)
            }
        }
        btnBack.setOnClickListener {
            if (stepNumber > 1)
                showCurrentStep(--stepNumber)
        }
    }

    override fun onItemClick(model: Any?, position: Int) {
//        list.clear()
//        list.addAll(DataFactory.getServicesList())
//        list.get(position).isSelected = true
//        recyclerServices.adapter?.notifyDataSetChanged()
    }

    private fun showCurrentStep(step: Int) {
        setPageNo()
        when (step) {
            1 -> {
                recyclerServices.initGrid(mContext!!)
                recyclerServices.adapter =
                    ServicesAdapter(mContext!!, list, R.layout.item_service, this)
            }
            2 -> {
                layoutList.visibility = View.VISIBLE
                recyclerServices.initVertical(mContext!!)
                recyclerServices.adapter = StylistAdapter(mContext!!, stylistList, this)
                layoutCalender.visibility = View.GONE
            }
            3 -> {

                layoutList.visibility = View.GONE
                layoutCalender.visibility = View.VISIBLE
                recyclerTimeSlots.initGrid(mContext!!)
                recyclerTimeSlots.adapter = TimeSlotAdapter(mContext!!, timeList, this)
            }
            4 -> {

                layoutList.visibility = View.GONE
                layoutCalender.visibility = View.GONE
                layoutThankYou.visibility = View.VISIBLE
                layoutButtons.visibility = View.GONE
                btnGotoAptmnt.setOnClickListener {
                    replaceFragment(AppointmentsFragment())
                }
            }

        }
    }

    private fun setPageNo() {
        txtPgno.text = "$stepNumber of 4"
    }
}